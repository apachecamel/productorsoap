package hello;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import io.spring.guides.gs_producing_web_service.Cuenta;

@Component
public class CuentaRepository {
	public static final Map<String, Cuenta> cuentas = new HashMap<>();
	
	@PostConstruct
	public void initDB(){
		
		Cuenta amu = new Cuenta();
		amu.setName("Amilcar");
		amu.setCategoria(9);
		amu.setSaldo(99999);
		amu.setBanco("Frances");
		
		Cuenta ignacio = new Cuenta();
		ignacio.setName("Ignacio");
		ignacio.setCategoria(1);
		ignacio.setSaldo(20);
		ignacio.setBanco("Galicia");
		
		cuentas.put(amu.getName(), amu);
		cuentas.put(ignacio.getName(), ignacio);
		
	}
	
	public Cuenta findCuenta(String name) {
		
		return cuentas.get(name);
	}
	
	public Cuenta findHardcode() {
		
		return cuentas.get("Ignacio");
	}

}
