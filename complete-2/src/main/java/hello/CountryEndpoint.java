package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.guides.gs_producing_web_service.GetCountryRequest;
import io.spring.guides.gs_producing_web_service.GetCountryResponse;
import io.spring.guides.gs_producing_web_service.GetCuentaRequest;
import io.spring.guides.gs_producing_web_service.GetCuentaResponse;
import io.spring.guides.gs_producing_web_service.GetHardcodeResponse;

@Endpoint
public class CountryEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	private CountryRepository countryRepository;
	
	private CuentaRepository cuentaRepository;

	@Autowired
	public CountryEndpoint(CountryRepository countryRepository, CuentaRepository cuentaRepository) {
		this.countryRepository = countryRepository;
		this.cuentaRepository = cuentaRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
	@ResponsePayload
	public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
		GetCountryResponse response = new GetCountryResponse();
		response.setCountry(countryRepository.findCountry(request.getName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCuentaRequest")
	@ResponsePayload
	public GetCuentaResponse getCuenta(@RequestPayload GetCuentaRequest request) {
		
		GetCuentaResponse response = new GetCuentaResponse();
		response.setCuenta(cuentaRepository.findCuenta(request.getName()));
		
		System.out.println("----pego bien en getCuenta------");
		System.out.println("---nombre: "+request.getName()+"---");
		
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getHardcodeRequest")
	@ResponsePayload
	public GetHardcodeResponse getHardcode() {
		
		GetHardcodeResponse response = new GetHardcodeResponse();
		response.setCuenta(cuentaRepository.findHardcode());
		
		System.out.println("-----pego bien-----");
		
		return response;
	}
	

}
